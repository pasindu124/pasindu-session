package com.java.pasindu.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name= "student")
@Getter
@Setter
public class Student implements Serializable {

	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long id;

	@Column(name = "firstname")
	private String firstName;

	@Column(name = "lastname")
	private String lastName;

	@Column(name = "birthday")
	private Date birthDay;

//	@OneToMany(mappedBy="student", fetch = FetchType.LAZY, orphanRemoval = false)
//	private List<StudentAddress> studentAddressList;

}
