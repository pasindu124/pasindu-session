package com.java.pasindu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class PasinduApplication {

	public static void main(String[] args) {
		SpringApplication.run(PasinduApplication.class, args);
	}

}
