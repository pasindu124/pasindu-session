package com.java.pasindu.dao;

import com.java.pasindu.model.Student;
import com.java.pasindu.model.StudentAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends JpaRepository<StudentAddress, Long> {

	List<StudentAddress> findByStudentId(Long id);


}
