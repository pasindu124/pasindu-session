package com.java.pasindu.controller;

import com.java.pasindu.dao.AddressRepository;
import com.java.pasindu.model.Student;
import com.java.pasindu.dao.StudentRepository;
import com.java.pasindu.model.StudentAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {

	@Autowired StudentRepository studentRepository;

	@Autowired AddressRepository addressRepository;

	Logger logger = LoggerFactory.getLogger(StudentController.class);

	@PostMapping("/student")
	public Student insertStudent(@RequestBody Student student) {
		return studentRepository.save(student);
	}

	@GetMapping("/students")
	public List<Student> getAllStudents() {
		return studentRepository.findAll();
	}

	@PostMapping("/delete_students")
	public void deleteAllStudents() {
		studentRepository.deleteAll();
	}

	@GetMapping("/getAddresses")
	public List<StudentAddress> getAddresses(@RequestParam String id) {
		return getAddressesByStudentId(id);
	}

	@Cacheable(value="cacheStudentInfo")
	public List<StudentAddress> getAddressesByStudentId(String id) {
		return addressRepository.findByStudentId(Long.parseLong(id));
	}


	@GetMapping("/testLogs")
	public String index() {
		logger.trace("A TRACE Message");
		logger.debug("A DEBUG Message");
		logger.info("An INFO Message");
		logger.warn("A WARN Message");
		logger.error("An ERROR Message");

		return "Howdy! Check out the Logs to see the output...";
	}





}
